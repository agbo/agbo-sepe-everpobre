//
//  AGTNoteViewController.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 05/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AGTNote;

@interface AGTNoteViewController : UITableViewController


@property (nonatomic, strong) AGTNote *note;

-(id) initWithModel:(AGTNote*) model;

@end
