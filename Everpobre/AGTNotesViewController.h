//
//  AGTNotesViewController.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 04/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTCoreDataTableViewController.h"
#import "AGTNotebook.h"

@interface AGTNotesViewController : AGTCoreDataTableViewController

@property (strong, nonatomic) AGTNotebook *notebook;

@end
