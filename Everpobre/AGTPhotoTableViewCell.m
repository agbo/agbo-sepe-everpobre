//
//  AGTPhotoTableViewCell.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 05/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTPhotoTableViewCell.h"
#import "AGTNote.h"
#import "AGTPhotoContainer.h"

@implementation AGTPhotoTableViewCell

#pragma mark - Properties
-(void) setNote:(AGTNote *)note{
    
    // Guardamos la nota
    _note = note;
    
    // Sincronizamos los datos de la nota con la vista
    if (note.photo.image) {
        self.photoView.image = note.photo.image;
    }
    
    
}

#pragma mark - Class Mehtods
+(CGFloat)height{
    
    return 320.0f;
}

+(NSString*)cellId{
    return [self description];
}



- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
