//
//  AGTNotebookTableViewCell.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 05/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGTNotebookTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *nameView;
@property (weak, nonatomic) IBOutlet UILabel *notesView;


+(CGFloat)height;
+(NSString*)cellId;


@end
