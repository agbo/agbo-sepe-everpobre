//
//  AGTNotebooksViewController.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 04/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTNotebooksViewController.h"
#import "AGTNotebook.h"
#import "AGTNote.h"
#import "AGTNotesViewController.h"
#import "AGTNotebookTableViewCell.h"

@interface AGTNotebooksViewController ()

@end

@implementation AGTNotebooksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Everpobre";
    
    // Añadimos botón de nueva libreta
    UIBarButtonItem *addBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNotebook:)];
    
    self.navigationItem.rightBarButtonItem = addBtn;
    
    
    // Registramos el nib de la celda
    UINib *nib = [UINib nibWithNibName:@"AGTNotebookTableViewCell" bundle:[NSBundle mainBundle]];
    
    [self.tableView registerNib:nib forCellReuseIdentifier:[AGTNotebookTableViewCell cellId]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell*) tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // Averiguamos de qué libreta me hablan
    AGTNotebook *nb = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Creo una celda
    AGTNotebookTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[AGTNotebookTableViewCell cellId] forIndexPath:indexPath];
    
    
    // La configuro (sincronizo modelo -> vista)
    cell.nameView.text = nb.name;
    cell.notesView.text = [NSString stringWithFormat: @"%lu",nb.notes.count];
    
  
    // La devuelvo
    return cell;
    
}

-(CGFloat) tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [AGTNotebookTableViewCell height];
    
}

-(void) tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    // Averiguar si el pollo quiere eliminar
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        // Borramos la libreta, pero ¿cuala es?
        NSManagedObjectContext *ctx = self.fetchedResultsController.managedObjectContext;
        
        AGTNotebook *difunto = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        [ctx deleteObject:difunto];
        
    }
    
}


-(NSString *) tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return @"Remove";
}


#pragma mark - Delegate
-(void) tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Averiguar cuala fue la libreta
    AGTNotebook *nb = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Creo la selección de datos
    NSFetchRequest *r = [NSFetchRequest fetchRequestWithEntityName:[AGTNote entityName]];
    
    r.fetchBatchSize = 30;
    r.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:AGTNoteAttributes.name ascending:YES selector:@selector(caseInsensitiveCompare:)],
                          [NSSortDescriptor sortDescriptorWithKey:AGTNoteAttributes.modificationDate ascending:NO]];
    
    r.predicate = [NSPredicate predicateWithFormat:@"notebook == %@", nb];
    
    NSFetchedResultsController *fc = [[NSFetchedResultsController alloc]initWithFetchRequest:r managedObjectContext:self.fetchedResultsController.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    
    
    // Creo una instancia de contr. de notas
    AGTNotesViewController *nVC = [[AGTNotesViewController alloc] initWithFetchedResultsController:fc style:UITableViewStylePlain];
    
    // Le asigno su libreta, pa que lo sepa
    nVC.notebook = nb;
    
    // Lo pusheo
    [self.navigationController pushViewController:nVC
                                         animated:YES];
    
    
}

#pragma mark -  Actions
-(void)addNotebook:(id) sender{
    
    [AGTNotebook notebookWithName:@"Nueva libreta" context:self.fetchedResultsController.managedObjectContext];
}














@end
