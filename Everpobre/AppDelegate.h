//
//  AppDelegate.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 02/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;



@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

