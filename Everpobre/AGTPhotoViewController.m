//
//  AGTPhotoViewController.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 05/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import CoreImage;

#import "AGTPhotoViewController.h"
#import "AGTNote.h"
#import "AGTPhotoContainer.h"
#import "UIImage+Resize.h"

@interface AGTPhotoViewController ()
@property (nonatomic, strong) AGTNote *model;
@end

@implementation AGTPhotoViewController

-(id) initWithModel:(AGTNote*) model{
    
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _model = model;
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // sincornizamos modelo -> vista
    self.photoView.image = self.model.photo.image;
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    // sincronizamos vista -> modelo
    self.model.photo.image = self.photoView.image;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)deletePhoto:(id)sender {
    
    CGRect oldRect = self.photoView.bounds;
    
    // Animación
    [UIView animateWithDuration:0.8
                          delay:0
                        options:0
                     animations:^{
                         
                         // Estado final (se va animar)
                         self.photoView.bounds = CGRectZero;
                         self.photoView.alpha = 0;
                         
                     } completion:^(BOOL finished) {
                         
                         // Quitar la foto del  modelo
                         self.model.photo.image = nil;
                         
                         // Quitar la foto de la vista
                         self.photoView.image = nil;
                         
                         // Dejamos la vista como estaba
                         self.photoView.bounds = oldRect;
                         self.photoView.alpha = 1.0;
                     }];
    
    
    [UIView animateWithDuration:0.8
                          delay:0
         usingSpringWithDamping:0.4
          initialSpringVelocity:0.4
                        options:0
                     animations:^{
                         // transformada afin
                         self.photoView.transform = CGAffineTransformMakeRotation(M_PI_2);
                         
                     } completion:^(BOOL finished) {
                         self.photoView.transform = CGAffineTransformIdentity;
                     }];
    
}

- (IBAction)takePhoto:(id)sender {
    
    // Crear un Picker
    UIImagePickerController *picker = [UIImagePickerController new];
    
    // Configurar
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        // tenemos camara
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }else{
        // pos nos conformamos con el carrete
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    picker.delegate = self;
    picker.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    // Presentar
    [self presentViewController:picker
                       animated:YES
                     completion:^{
                         NSLog(@"¡terminé!");
                     }];
    
   
}


- (IBAction)vintagify:(id)sender {
    
    // Creamos un contexto
    CIContext *context = [CIContext contextWithOptions:nil];
    
    // Obtenemos la imagen original
    CIImage *original = [CIImage imageWithCGImage:self.model.photo.image.CGImage];
    
    // Creamos y configuramos el filtro
    CIFilter *falseColor = [CIFilter filterWithName:@"CIFalseColor"];
    [falseColor setDefaults];
    [falseColor setValue:original
                  forKey:@"InputImage"];
    
    
    // Obtengo la imagen de salida
    CIImage *output = falseColor.outputImage;
    
    
    // Creamos el filtro de viñeta
    CIFilter *vignette = [CIFilter filterWithName:@"CIVignette"];
    [vignette setDefaults];
    [vignette setValue:@12
                forKey:@"InputIntensity"];
    
    // Los encadenamos
    [vignette setValue:output
                forKey:@"InputImage"];
    
    output = vignette.outputImage;
    
    // Aplicamos los filtros
    CGImageRef final = [context createCGImage:output
                                     fromRect:[output extent]];
    
    // Obtengo la imagen en forma de UIImage
    UIImage *finalImg = [UIImage imageWithCGImage:final];
    
    // Liberamos recursos
    CGImageRelease(final);
    
    // Encasquetamos en UIImageView
    self.photoView.image = finalImg;
    self.model.photo.image = finalImg;
    
    
    
    
}



#pragma mark - UIImagePickerControllerDelegate
-(void) imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    // ¡OJO! Pico de memoria
    UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    // reducimos la imagen para que tenga la mitad de
    // del tamaño original. Lo correcto sería que coincida
    // con el tamaño de la pantalla, pero habría que calcular
    // la escala correctamente: deberes pal finde.
    CGSize newSize = CGSizeMake(img.size.width * 0.5, img.size.height *0.5);
    // Este método, con imágenes grandes y máxima calidad,
    // va a tardar un poco. Lo ideal sería pasarlo a
    // segundo plano con GCD. deberes pal finde.
    img = [img resizedImage:newSize
       interpolationQuality:kCGInterpolationHigh];
    
    self.model.photo.image = img;
    
    
    // ocultar el picker
    [self dismissViewControllerAnimated:YES
                             completion:nil];
    
}












@end
