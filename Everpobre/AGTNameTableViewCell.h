//
//  AGTNameTableViewCell.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 05/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;

@class AGTNote;

@interface AGTNameTableViewCell : UITableViewCell

+(CGFloat)height;
+(NSString*)cellId;

@property (weak, nonatomic) IBOutlet UITextField *nameField;

@property (nonatomic, strong) AGTNote *note;



@end
