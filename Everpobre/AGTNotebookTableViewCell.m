//
//  AGTNotebookTableViewCell.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 05/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTNotebookTableViewCell.h"

@implementation AGTNotebookTableViewCell

+(CGFloat)height{
    return 60.0f;
}
+(NSString*)cellId{
    return [self description];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}






@end
