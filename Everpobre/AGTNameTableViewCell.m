//
//  AGTNameTableViewCell.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 05/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTNameTableViewCell.h"
#import "AGTNote.h"

@interface AGTNameTableViewCell ()

@end

@implementation AGTNameTableViewCell

#pragma mark - Properties
-(void) setNote:(AGTNote *)note{

    // sincronizamos la vista con la nota
    self.nameField.text = note.name;
    
    // guardamos la nota
    _note = note;
}

#pragma mark - Class Methods
+(CGFloat)height{
    return 44.0f;
}

+(NSString*)cellId{
    return [self description];
}


#pragma mark - Misc

-(void) prepareForReuse{
    [super prepareForReuse];
    
    // Cuando desaparezco, me mandan este mensaje
    // pa que me resetee y me prepare para ser reutilizado. Algo asín como un viewWillDissappear para celdas
    
    // Sincroniza la vista con la nota por si
    // hubo cambios
    
    self.note.name = self.nameField.text;
}



- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
