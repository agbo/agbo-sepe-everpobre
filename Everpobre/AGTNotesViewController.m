//
//  AGTNotesViewController.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 04/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTNotesViewController.h"
#import "AGTNote.h"
#import "AGTPhotoContainer.h"
#import "AGTNoteViewController.h"

@implementation AGTNotesViewController

-(void) viewDidLoad{
    [super viewDidLoad];
    
    // agregamos botón de añadir notas
    UIBarButtonItem *b = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNote:)];
    
    self.navigationItem.rightBarButtonItem = b;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.title = self.notebook.name;
    
}
-(UITableViewCell*) tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // Averiguar la nota
    AGTNote *n = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    // Creamos la celda
    static NSString *cellId = @"noteCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
    }
    
    // La configuramos
    cell.imageView.image = n.photo.image;
    cell.textLabel.text = n.name;
    NSDateFormatter *fmt = [NSDateFormatter new];
    fmt.dateStyle = NSDateIntervalFormatterMediumStyle;
    cell.detailTextLabel.text = [fmt stringFromDate:n.modificationDate];

    // La devolvemos
    return cell;

}

-(void) tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        // Averiguo la celda
        AGTNote *difunto = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        
        // la elimino
        NSManagedObjectContext *ctxt = self.notebook.managedObjectContext;
        
        [ctxt deleteObject:difunto];
    }
}

-(NSString *) tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return @"Remove";
}


#pragma mark -  Actions
-(void) addNote:(id) sender{
    
    [AGTNote noteWithName:@"Nueva nota"
                 notebook:self.notebook
                  context:self.notebook.managedObjectContext];
    
    
}


#pragma mark -  Delegate
-(void) tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // Averiguar la nota
    AGTNote *note = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    // Crear formulario de nota
    AGTNoteViewController *nVC = [[AGTNoteViewController alloc] initWithModel:note];
    
    // Hacer le push
    [self.navigationController pushViewController:nVC
                                         animated:YES];
    
    
}

















@end
