//
//  AGTTextTableViewCell.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 05/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTTextTableViewCell.h"
#import "AGTNote.h"

@implementation AGTTextTableViewCell

#pragma mark - Class Methods
+(CGFloat)height{
    return 320.0f;
}
+(NSString*)cellId{
    return [self description];
}

#pragma mark - Properties
-(void) setNote:(AGTNote *)note{
    
    // Guardamos la nota
    _note = note;
    
    // mostramos el texto en la vista
    self.textView.text = note.text;
}

#pragma mark -  Misc
-(void) prepareForReuse{
    [super prepareForReuse];
    
    // guardo lo que haya escrito el usuario
    // en la nota
    self.note.text = self.textView.text;
    
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
