//
//  AGTPhotoViewController.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 05/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AGTNote;

@interface AGTPhotoViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
- (IBAction)vintagify:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *photoView;
- (IBAction)deletePhoto:(id)sender;
- (IBAction)takePhoto:(id)sender;


-(id) initWithModel:(AGTNote*) model;


@end
