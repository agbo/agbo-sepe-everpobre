//
//  AppDelegate.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 02/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AppDelegate.h"
#import "AGTCoreDataStack.h"
#import "AGTNotebook.h"
#import "AGTNote.h"
#import "AGTPhotoContainer.h"
#import "AGTNotebooksViewController.h"
#import "UIViewController+Navigation.h"
#import "Settings.h"

@interface AppDelegate ()
@property (nonatomic, strong)AGTCoreDataStack *stack;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    
    // Creamos el stack
    self.stack = [AGTCoreDataStack coreDataStackWithModelName:@"Model"];
    
    
    // Creamos datos chorras
    //[self createDummyData];
    
    // Creamos el conjunto de datos
    NSFetchRequest *r = [NSFetchRequest fetchRequestWithEntityName:[AGTNotebook entityName]];
    r.fetchBatchSize = 30;
    r.sortDescriptors = @[
                          [NSSortDescriptor sortDescriptorWithKey:AGTNoteAttributes.name ascending:YES selector:@selector(caseInsensitiveCompare:)],
                          [NSSortDescriptor sortDescriptorWithKey:AGTNoteAttributes.modificationDate ascending:NO]];
    
    NSFetchedResultsController *fc = [[NSFetchedResultsController alloc]initWithFetchRequest:r managedObjectContext:self.stack.context sectionNameKeyPath:nil cacheName:nil];
    
    
    // Creamos el controlador
    AGTNotebooksViewController *nbVC = [[AGTNotebooksViewController alloc] initWithFetchedResultsController:fc style:UITableViewStylePlain];
    
    
    // Arrancamos el autoguardado
    [self autoSave];

    
    // Lo mostramos
    self.window.rootViewController = [nbVC agtWrappedInNavigation];
    
    
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self.stack saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Pos estamos jodidos: %@", error);
    }];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [self.stack saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Pos estamos jodidos: %@", error);
    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    
}

-(void) createDummyData{
    
    [self.stack zapAllData];
    
   AGTNotebook *nb = [AGTNotebook notebookWithName:@"Ex-novias para el recuerdo"
                                           context:self.stack.context];
    
    [AGTNote noteWithName:@"Mariana Dávalos"
                 notebook:nb
                  context:self.stack.context];
    
    [AGTNote noteWithName:@"Camila Dávalos"
                 notebook:nb
                  context:self.stack.context];
    
    [AGTNote noteWithName:@"Pampita"
                 notebook:nb
                  context:self.stack.context];
    

}




-(void) trastearConDatos{
    
    AGTNotebook *apps = [AGTNotebook notebookWithName:@"Ideas de Apps"
                                              context:self.stack.context];
    
    AGTNote *iCachete = [AGTNote noteWithName:@"iCachete"
                                     notebook:apps
                                      context:self.stack.context];
    
    
    // Comprobar que modificationDate se actualiza
    NSLog(@"Antes: %@", iCachete.modificationDate);
    iCachete.text = @"App educativa para reforzar la coordinación motora fina y los reflejos";
    
    NSLog(@"Después: %@", iCachete.modificationDate);
    
    
    // Búsqueda
    NSFetchRequest *r = [NSFetchRequest fetchRequestWithEntityName:[AGTNote entityName]];
    r.fetchBatchSize = 20;
    r.sortDescriptors = @[
                          [NSSortDescriptor
                           sortDescriptorWithKey:AGTNoteAttributes.name ascending:YES selector:@selector(caseInsensitiveCompare:)],
                           [NSSortDescriptor
                            sortDescriptorWithKey:AGTNoteAttributes.modificationDate
                            ascending:NO]];
    
    
    r.predicate = [NSPredicate
                   predicateWithFormat:@"notebook == %@", apps];
    
    
    NSError *err = nil;
    NSArray *res = [self.stack.context executeFetchRequest:r
                                                     error:&err];
    
    if (res == nil) {
        // la cagamos
        NSLog(@"Error al buscar: %@", err);
    }
    
    NSLog(@"Número de libretas: %lu", (unsigned long)[res count]);
    
    NSLog(@"las libretas: %@", res);
    
    // ¿De verdad es un array?
    NSLog(@"Clase: %@", [res class]);
    
    // Borrar
    [self.stack.context deleteObject:apps]; // borro la libreta.
    
    r.predicate = nil;
    res = [self.stack.context executeFetchRequest:r
                                            error:&err];
    if (res == nil) {
        NSLog(@"Errro al buscar de nuevo: %@", res);
    }
    NSLog(@"Notas existentes: %@", res);
    
    // Guardamos
    [self.stack saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Error al guardar, cagüentó. %@", error);
    }];
    
    
    
}

#pragma mark -  Autosave
-(void) autoSave{
    
    if (AUTO_SAVE) {
        NSLog(@"Auto guardando");
        
        [self.stack saveWithErrorBlock:^(NSError *error) {
            NSLog(@"Error al auto guardar! %@", error);
        }];
        
        [self performSelector:@selector(autoSave)
                   withObject:nil
                   afterDelay:AUTO_SAVE_DELAY];
        
    }
}

















@end
