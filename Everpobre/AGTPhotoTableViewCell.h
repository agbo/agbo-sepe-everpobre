//
//  AGTPhotoTableViewCell.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 05/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AGTNote;

@interface AGTPhotoTableViewCell : UITableViewCell

+(CGFloat)height;
+(NSString*)cellId;


@property (weak, nonatomic) IBOutlet UIImageView *photoView;

@property (nonatomic, strong) AGTNote *note;


@end
