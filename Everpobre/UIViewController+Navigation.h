//
//  UIViewController+Navigation.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 04/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Navigation)

-(UINavigationController *) agtWrappedInNavigation;
@end
