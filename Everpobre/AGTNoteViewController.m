//
//  AGTNoteViewController.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 05/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

// Constantes
#define NUMBER_OF_SECTIONS 4
#define NAME_SECTION_HEADER @"Name"
#define DATES_SECTION_HEADER @"Created and last modified"
#define TEXT_SECTION_HEADER @"Text"
#define PHOTO_SECTION_HEADER @"Photo"
#define NAME_SECTION    0
#define DATES_SECTION   1
#define TEXT_SECTION    2
#define PHOTO_SECTION   3

#import "AGTNoteViewController.h"
#import "AGTNote.h"
#import "AGTNameTableViewCell.h"
#import "AGTTextTableViewCell.h"
#import "AGTDatesTableViewCell.h"
#import "AGTPhotoTableViewCell.h"
#import "AGTPhotoViewController.h"




@interface AGTNoteViewController ()

@end

@implementation AGTNoteViewController

-(id) initWithModel:(AGTNote*) model{
    
    if(self = [super initWithNibName:nil bundle:nil]){
        
        _note = model;
    }
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // registrar las celdas
    [self registerNibs];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return NUMBER_OF_SECTIONS;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

-(NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    switch (section) {
        case NAME_SECTION:
            return NAME_SECTION_HEADER;
            break;
            
        case DATES_SECTION:
            return DATES_SECTION_HEADER;
            break;
            
        case TEXT_SECTION:
            return TEXT_SECTION_HEADER;
            break;
            
        case PHOTO_SECTION:
            return PHOTO_SECTION_HEADER;
            break;
            
        default:
            [NSException raise:@"Section no existe" format:nil];
            return @"esto no existe";
            break;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // Averiguamos de qué celda nos habla
    switch (indexPath.section) {
        case NAME_SECTION:{
            
            AGTNameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[AGTNameTableViewCell cellId] forIndexPath:indexPath];
            
            [cell setNote:self.note];
            return cell;
            break;}
         
        case DATES_SECTION:{
            
            AGTDatesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[AGTDatesTableViewCell cellId] forIndexPath:indexPath];
            [cell setNote:self.note];
            return cell;
            break;}
        
        case TEXT_SECTION:{
            
            AGTTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[AGTTextTableViewCell cellId] forIndexPath:indexPath];
            [cell setNote:self.note];
            return cell;
            break;}
        
        case PHOTO_SECTION:{
            
            AGTPhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[AGTPhotoTableViewCell cellId] forIndexPath:indexPath];
            [cell setNote:self.note];
            return cell;
            break;}
            
        default:
            [NSException raise:@"UnkownSection"
                        format:@"Unknown section %ld",(long)indexPath.section];
            return nil;
            break;
    }
}


-(CGFloat) tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case NAME_SECTION:
            return [AGTNameTableViewCell height];
            break;
           
            
        case DATES_SECTION:
            return [AGTDatesTableViewCell height];
            break;
         
        case TEXT_SECTION:
            return [AGTTextTableViewCell height];
            break;
            
        case PHOTO_SECTION:
            return [AGTPhotoTableViewCell height];
            break;
            
        default:
            return 0;
            break;
    }
}


-(void) registerNibs{
    
    NSBundle *main = [NSBundle mainBundle];
    
    UINib *nameNib = [UINib nibWithNibName:@"AGTNameTableViewCell"
                                    bundle:main];
    [self.tableView registerNib:nameNib forCellReuseIdentifier:[AGTNameTableViewCell cellId]];
    
    
    UINib *datesNib = [UINib nibWithNibName:@"AGTDatesTableViewCell" bundle:main];
    [self.tableView registerNib:datesNib forCellReuseIdentifier:[AGTDatesTableViewCell  cellId]];
    
    UINib *textNib = [UINib nibWithNibName:@"AGTTextTableViewCell" bundle:main];
    
    [self.tableView registerNib:textNib forCellReuseIdentifier:[AGTTextTableViewCell cellId]];
    
    UINib *photoNib = [UINib nibWithNibName:@"AGTPhotoTableViewCell" bundle:main];
    [self.tableView registerNib:photoNib forCellReuseIdentifier:[AGTPhotoTableViewCell cellId]];
  
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == PHOTO_SECTION) {
        
        
        // Mostramos el controlador de Fotos
        AGTPhotoViewController *pVC = [[AGTPhotoViewController alloc] initWithModel:self.note];
        
        [self.navigationController pushViewController:pVC
                                             animated:YES];
        
    }
}






@end






















