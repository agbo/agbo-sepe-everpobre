// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTPhotoContainer.m instead.

#import "_AGTPhotoContainer.h"

const struct AGTPhotoContainerAttributes AGTPhotoContainerAttributes = {
	.photoData = @"photoData",
};

const struct AGTPhotoContainerRelationships AGTPhotoContainerRelationships = {
	.notes = @"notes",
};

@implementation AGTPhotoContainerID
@end

@implementation _AGTPhotoContainer

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PhotoContainer" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PhotoContainer";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PhotoContainer" inManagedObjectContext:moc_];
}

- (AGTPhotoContainerID*)objectID {
	return (AGTPhotoContainerID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic photoData;

@dynamic notes;

- (NSMutableSet*)notesSet {
	[self willAccessValueForKey:@"notes"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"notes"];

	[self didAccessValueForKey:@"notes"];
	return result;
}

@end

