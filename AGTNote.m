#import "AGTNote.h"
#import "AGTPhotoContainer.h"
#import "AGTNotebook.h"

@interface AGTNote ()

// Private interface goes here.

@end

@implementation AGTNote

+(instancetype) noteWithName:(NSString*) name
                    notebook:(AGTNotebook*) notebook
                     context:(NSManagedObjectContext*) context{
    
    
    AGTNote *note = [self insertInManagedObjectContext:context];
    
    note.name = name;
    note.notebook = notebook;
    note.creationDate = [NSDate date];
    note.photo = [AGTPhotoContainer insertInManagedObjectContext:context];
    note.modificationDate = [NSDate date];
    
    return note;
}




#pragma mark - Util
-(NSArray *) observableKeys{
    return @[AGTNoteAttributes.name,
             AGTNoteAttributes.text,
             AGTNoteRelationships.notebook,
             AGTNoteRelationships.photo,
             @"photo.photoData"];
}
#pragma mark - KVO
-(void) observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context{
    
    // Solo me interesa saber que algo ha cambiado
    self.modificationDate = [NSDate date];
}






@end
