//
//  AGTEverpobreBaseClass.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 03/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import CoreData;

@interface AGTEverpobreBaseClass : NSManagedObject

-(NSArray *) observableKeys;
-(void) setupKVO;
-(void) tearDownKVO;

@end
