#import "AGTPhotoContainer.h"

@interface AGTPhotoContainer ()

// Private interface goes here.

@end

@implementation AGTPhotoContainer

// Custom logic goes here.

-(UIImage*)image{
    
    return [UIImage imageWithData:self.photoData];
}

-(void) setImage:(UIImage *)image{
    
    self.photoData = UIImageJPEGRepresentation(image, 0.9);
}
@end
