// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTPhotoContainer.h instead.

@import CoreData;
#import "AGTEverpobreBaseClass.h"

extern const struct AGTPhotoContainerAttributes {
	__unsafe_unretained NSString *photoData;
} AGTPhotoContainerAttributes;

extern const struct AGTPhotoContainerRelationships {
	__unsafe_unretained NSString *notes;
} AGTPhotoContainerRelationships;

@class AGTNote;

@interface AGTPhotoContainerID : NSManagedObjectID {}
@end

@interface _AGTPhotoContainer : AGTEverpobreBaseClass {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AGTPhotoContainerID* objectID;

@property (nonatomic, strong) NSData* photoData;

//- (BOOL)validatePhotoData:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *notes;

- (NSMutableSet*)notesSet;

@end

@interface _AGTPhotoContainer (NotesCoreDataGeneratedAccessors)
- (void)addNotes:(NSSet*)value_;
- (void)removeNotes:(NSSet*)value_;
- (void)addNotesObject:(AGTNote*)value_;
- (void)removeNotesObject:(AGTNote*)value_;

@end

@interface _AGTPhotoContainer (CoreDataGeneratedPrimitiveAccessors)

- (NSData*)primitivePhotoData;
- (void)setPrimitivePhotoData:(NSData*)value;

- (NSMutableSet*)primitiveNotes;
- (void)setPrimitiveNotes:(NSMutableSet*)value;

@end
