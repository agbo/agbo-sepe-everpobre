#import "_AGTPhotoContainer.h"

@import UIKit;

@interface AGTPhotoContainer : _AGTPhotoContainer {}
// Custom logic goes here.

@property (nonatomic, strong) UIImage *image;

@end
