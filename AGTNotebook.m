#import "AGTNotebook.h"

@interface AGTNotebook ()

// Private interface goes here.

@end

@implementation AGTNotebook

+(instancetype)notebookWithName:(NSString*) name
                        context:(NSManagedObjectContext*) context{
    
    AGTNotebook *nb = [self insertInManagedObjectContext:context];
    
    nb.name = name;
    nb.creationDate = [NSDate date];
    nb.modificationDate = [NSDate date];
    
    return nb;
}



#pragma mark - Util
-(NSArray *) observableKeys{
    return @[AGTNotebookAttributes.name,
             AGTNotebookRelationships.notes];
}

#pragma mark - KVO
-(void) observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context{
    
    // Solo me interesa saber que algo ha cambiado
    self.modificationDate = [NSDate date];
}











@end





